package ru.tsc.izuev.tm.model;

public final class Command {

    private String name;

    private String argument;

    private String description;

    public Command() {
    }

    public Command(
            final String name,
            final String argument,
            final String description
    ) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        final boolean hasName = name != null && !name.isEmpty();
        if (hasName) result += name;
        if (argument != null && !argument.isEmpty()) result += hasName ? ", " + argument : argument;
        if (description != null && !description.isEmpty()) result += " : " + description;
        return result;
    }

}
