package ru.tsc.izuev.tm.api.controller;

public interface ITaskController {

    void createTask();

    void updateTaskById();

    void updateTaskByIndex();

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

}
